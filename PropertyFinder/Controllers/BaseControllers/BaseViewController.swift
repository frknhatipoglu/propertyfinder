//
//  BaseViewController.swift
//  PropertyFinder
//
//  Created by Scorp on 20.07.2020.
//

import UIKit

class BaseViewController: UIViewController {

    public lazy var activityIndicatorView: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView()
        activityIndicatorView.style = .large
        activityIndicatorView.isUserInteractionEnabled = false
        activityIndicatorView.color = UIColor.darkGray
        activityIndicatorView.layer.zPosition = 1
        return activityIndicatorView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func setActivityIndicatiorView () {
        view.addSubview(activityIndicatorView)
        activityIndicatorView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
    }

}
