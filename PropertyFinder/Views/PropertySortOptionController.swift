//
//  PropertySortOptionController.swift
//  PropertyFinder
//
//  Created by Scorp on 21.07.2020.
//

import UIKit

class PropertySortOptionController: UIControl {
    
    private lazy var checkImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "checkIcon")?.withRenderingMode(.alwaysOriginal)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.textColor = UIColor.black
        return label
    }()
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                self.backgroundColor = UIColor.Custom.NavigationBar.tint
                self.titleLabel.textColor = UIColor.white
                self.checkImageView.isHidden = false
            } else {
                self.backgroundColor = UIColor.clear
                self.titleLabel.textColor = UIColor.black
                self.checkImageView.isHidden = true
            }
        }
    }
    
    //MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Constructers
    private func configureUI () {
        
        checkImageView.isHidden = true
        self.addSubview(checkImageView)
        checkImageView.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(15)
            make.centerY.equalToSuperview()
            make.size.equalTo(30)
        }
        
        self.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(checkImageView.snp.trailing).offset(5)
            make.top.bottom.trailing.equalToSuperview().inset(20)
        }
    }
}

extension PropertySortOptionController {
    public func set (title: String?) {
        titleLabel.text = title
    }
}
