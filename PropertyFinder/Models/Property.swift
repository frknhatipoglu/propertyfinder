//
//  SearchResponse.swift
//  PropertyFinder
//
//  Created by Scorp on 21.07.2020.
//

import Foundation

// MARK: - SearchResponse
@objcMembers class SearchResponse: NSObject, Codable {
    let total: Int?
    let res: [Property]?

    init(total: Int?, res: [Property]?) {
        self.total = total
        self.res = res
    }
}

// MARK: - Re
@objcMembers class Property: NSObject, Codable {
    let id: Int?
    let title, subject: String?
    let thumbnail, thumbnailBig: String?
    let price: String?
    let currency: String?

    enum CodingKeys: String, CodingKey {
        case id
        case title, subject
        case thumbnail
        case thumbnailBig = "thumbnail_big"
        case price
        case currency

    }

    init(id: Int?, update: Int?, title: String?, subject: String?, thumbnail: String?, thumbnailBig: String?, price: String?, currency: String?) {
        self.id = id
        self.title = title
        self.subject = subject
        self.thumbnail = thumbnail
        self.thumbnailBig = thumbnailBig
        self.price = price
        self.currency = currency
    }
}

