//
//  API.swift
//  PropertyFinder
//
//  Created by Scorp on 21.07.2020.
//

import Foundation
import Alamofire

class API {
    
    private static let searchUrl = "https://www.propertyfinder.ae/mobileapi/search"
    
    public static func search (sortOption: String? = nil, pageIndex: Int? = nil, compHandler: @escaping (Data?, Error?) -> Void) {
        
        var parameters: Parameters = [String: Any]()
        parameters["ob"] = sortOption
        parameters["page"] = pageIndex
        
        AF.request(searchUrl,
                   method: .get,
                   parameters: parameters,
                   encoding: URLEncoding.default,
                   headers: nil,
                   interceptor: nil,
                   requestModifier: nil).response { (response) in
                    
                    compHandler(response.data, response.error)
        }
    }
}
