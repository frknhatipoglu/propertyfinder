//
//  InitialViewController.swift
//  PropertyFinder
//
//  Created by Scorp on 20.07.2020.
//

import UIKit
import SnapKit
import Kingfisher

class InitialViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        configureWindow()
    }
    
    private func configureUI () {
        self.view.backgroundColor = UIColor.Custom.MainViewController.background
    }
    
    private func configureWindow () {
        let mainNavigationController = getMainNavigationController(with: MainViewController())
        (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = mainNavigationController
    }
    
    private func getMainNavigationController (with viewController: UIViewController) -> UINavigationController {
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.navigationBar.barTintColor = UIColor.Custom.NavigationBar.tint
        navigationController.navigationBar.tintColor = UIColor.white
        navigationController.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController.navigationBar.barStyle = .black
        return navigationController
    }

}
