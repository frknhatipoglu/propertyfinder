//
//  UIColorExtensions.swift
//  PropertyFinder
//
//  Created by Scorp on 21.07.2020.
//

import UIKit

extension UIColor {
    struct Custom {
        struct NavigationBar {
            static let tint = UIColor(red: 222/255, green: 103/255, blue: 88/255, alpha: 1)
        }
        
        struct MainViewController {
            static let background = UIColor(red: 233/255, green: 233/255, blue: 233/255, alpha: 1)
        }
    }
}
