//
//  DataParser.swift
//  PropertyFinder
//
//  Created by Scorp on 21.07.2020.
//

import Foundation

class DataParser  {
    
    public static func parse (searchData: Data, compHandler: @escaping (SearchResponse?) -> ()) {
        
        //switch to background thread, dont block the main thread especially for larger datas
        DispatchQueue.global(qos: .userInitiated).async {
            let property = try? JSONDecoder().decode(SearchResponse.self, from: searchData)
            
            // return the callback on the main thread
            DispatchQueue.main.async {
                compHandler(property)
            }
        }
    }
}
