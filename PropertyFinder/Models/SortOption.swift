//
//  SortOptionTypes.swift
//  PropertyFinder
//
//  Created by Scorp on 21.07.2020.
//

import Foundation

enum SortOption: String {
    case priceDescending = "pd"
    case priceAscending = "pa"
    case bedsDescending = "bd"
    case bedsAscending = "ba"
}

extension SortOption {
    public func getDisplayName () -> String {
        switch self {
        case .priceDescending:
            return "Price Descending"
        case .priceAscending:
            return "Price Ascending"
        case .bedsDescending:
            return "Beds Descending"
        case .bedsAscending:
            return "Beds Ascending"
        }
    }
}
