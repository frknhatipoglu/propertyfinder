//
//  AppDelegate.swift
//  PropertyFinder
//
//  Created by Scorp on 20.07.2020.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        window = UIWindow()
        window?.rootViewController = InitialViewController()
        window?.makeKeyAndVisible()
        
        return true
    }
}

