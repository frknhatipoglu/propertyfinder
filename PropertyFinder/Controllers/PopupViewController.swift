//
//  PopupViewController.swift
//  PropertyFinder
//
//  Created by Scorp on 21.07.2020.
//

import UIKit

// MARK: - PopupViewControllerDelegate
protocol PopupViewControllerDelegate: class {
    func popupViewControllerWillDismiss ()
}

class PopupViewController: BaseViewController {
    
    // MARK: - Variables
    public var popupView: UIView?
    weak public var delegate: PopupViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        delegate?.popupViewControllerWillDismiss()
        super.dismiss(animated: flag, completion: completion)
    }
    
    private func configureUI () {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureTapped(_:)))
        tapGesture.delegate = self
        tapGesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapGesture)
        
        guard let popupView = popupView else {
            DispatchQueue.main.async {
                self.dismiss(animated: true, completion: nil)
            }
            return
        }
        
        view.addSubview(popupView)
        popupView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.equalTo(300)
        }
    }
    
    //MARK: - ACTIONS
    @objc private func tapGestureTapped (_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true)
    }
}

extension PopupViewController: UIGestureRecognizerDelegate {
    // cancel touched in subview (sort option UIControl)
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return touch.view == gestureRecognizer.view
    }
}
