//
//  PropertySortListView.swift
//  PropertyFinder
//
//  Created by Scorp on 21.07.2020.
//

import UIKit

class PropertySortOptionListView: UIView {
    
    private lazy var sortOptionStackView: UIStackView = {
        var stackView = UIStackView()
        stackView.alignment = .fill
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        return stackView
    }()
    
    private lazy var priceDescendingOption = PropertySortOptionController()
    private lazy var priceAscendingOption = PropertySortOptionController()
    private lazy var bedsDescendingOption = PropertySortOptionController()
    private lazy var bedsAscendingOption = PropertySortOptionController()
    
    private var selectedSortOption: SortOption?
    
    //MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Constructers
    private func configureUI () {
        self.backgroundColor = UIColor.white
        self.layer.cornerRadius = 10
        self.clipsToBounds = true
        
        self.addSubview(sortOptionStackView)
        sortOptionStackView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        priceDescendingOption.set(title: SortOption.priceDescending.getDisplayName())
        priceDescendingOption.addTarget(self, action: #selector(priceDescendingOptionTapped), for: .touchUpInside)
        sortOptionStackView.addArrangedSubview(priceDescendingOption)
        
        priceAscendingOption.set(title: SortOption.priceAscending.getDisplayName())
        priceAscendingOption.addTarget(self, action: #selector(priceAscendingOptionTapped), for: .touchUpInside)
        sortOptionStackView.addArrangedSubview(priceAscendingOption)
        
        bedsDescendingOption.set(title: SortOption.bedsDescending.getDisplayName())
        bedsDescendingOption.addTarget(self, action: #selector(bedsDescendingOptionTapped), for: .touchUpInside)
        sortOptionStackView.addArrangedSubview(bedsDescendingOption)
        
        bedsAscendingOption.set(title: SortOption.bedsAscending.getDisplayName())
        bedsAscendingOption.addTarget(self, action: #selector(bedsAscendingOptionTapped), for: .touchUpInside)
        sortOptionStackView.addArrangedSubview(bedsAscendingOption)
    }
    
    //MARK: - ACTIONS
    @objc private func priceDescendingOptionTapped () {
        
        clearSelections()
        
        if selectedSortOption == .priceDescending {
            selectedSortOption = nil
            return
        }
        
        priceDescendingOption.isSelected = true
        selectedSortOption = .priceDescending
    }
    
    @objc private func priceAscendingOptionTapped () {
        
        clearSelections()
        
        if selectedSortOption == .priceAscending {
            selectedSortOption = nil
            return
        }
        
        priceAscendingOption.isSelected = true
        selectedSortOption = .priceAscending
    }
    
    @objc private func bedsDescendingOptionTapped () {
        clearSelections()
        
        if selectedSortOption == .bedsDescending {
            selectedSortOption = nil
            return
        }
        
        bedsDescendingOption.isSelected = true
        selectedSortOption = .bedsDescending
    }
    
    @objc private func bedsAscendingOptionTapped () {
        clearSelections()
        
        if selectedSortOption == .bedsAscending {
            selectedSortOption = nil
            return
        }
        
        bedsAscendingOption.isSelected = true
        selectedSortOption = .bedsAscending
    }
    
    //MARK: - Helpers
    private func clearSelections () {
        priceDescendingOption.isSelected = false
        priceAscendingOption.isSelected = false
        bedsDescendingOption.isSelected = false
        bedsAscendingOption.isSelected = false
    }
}

extension PropertySortOptionListView {
    public func set (currentSortOption: SortOption?) {
        selectedSortOption = currentSortOption
    }
    
    public func getCurrentSortOption () -> SortOption? {
        return selectedSortOption
    }
}
