//
//  PropertyFinderTests.swift
//  PropertyFinderTests
//
//  Created by Scorp on 21.07.2020.
//

@testable import PropertyFinder
import XCTest

class PropertyFinderTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSearchAPI() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let waitExpectation = expectation(description: "Test search API")
        
        API.search { (data, error) in
            XCTAssertNil(error)
            XCTAssertNotNil(data)
            waitExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func testSearchAPIWithParameters() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let waitExpectation = expectation(description: "Test search API with parameters")
        
        API.search(sortOption: SortOption.bedsAscending.rawValue, pageIndex: 1) { (data, error) in
            XCTAssertNil(error)
            XCTAssertNotNil(data)
            waitExpectation.fulfill()
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func testDataParserWithLocalData() throws {
        
        let path = Bundle.main.path(forResource: "SearchResponseExample", ofType: "json")
        let data = try! Data(contentsOf: URL(fileURLWithPath: path!))
        
        let waitExpectation = expectation(description: "Test data parser with local data")
        
        DataParser.parse(searchData: data) { (response) in
            XCTAssertNotNil(data)
            waitExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func testDataParserWithRemoteData() throws {
        
        let waitExpectation = expectation(description: "Test data parser with remote data")
        
        API.search(sortOption: SortOption.priceAscending.rawValue, pageIndex: 1) { (data, error) in
            XCTAssertNil(error)
            XCTAssertNotNil(data)
            
            DataParser.parse(searchData: data!) { (response) in
                XCTAssertNotNil(data)
                waitExpectation.fulfill()
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }
}
