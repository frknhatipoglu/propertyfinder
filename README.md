# Property Finder App

Property Finder app is a demo to request Search API, parse the response, and display result.

## Getting Started

### Prerequisites

- Swift 5.0
- Xcode 11.4

Don't forget the work on .xcworkspace file.

### Folder Structure
├── Assets.xcassets
│   ├── AppIcon.appiconset
│   │   └── Contents.json
│   ├── Contents.json
│   ├── checkIcon.imageset
│   │   ├── Contents.json
│   │   ├── baseline_check_circle_outline_white_36pt_1x.png
│   │   ├── baseline_check_circle_outline_white_36pt_2x.png
│   │   └── baseline_check_circle_outline_white_36pt_3x.png
│   └── sortIcon.imageset
│       ├── Contents.json
│       ├── baseline_filter_list_white_36pt_1x.png
│       ├── baseline_filter_list_white_36pt_2x.png
│       └── baseline_filter_list_white_36pt_3x.png
├── Base.lproj
│   └── LaunchScreen.storyboard
├── Controllers
│   ├── BaseControllers
│   │   └── BaseViewController.swift
│   ├── MainViewController.swift
│   └── PopupViewController.swift
├── Delegates
│   └── AppDelegate.swift
├── Extensions
│   ├── UIColorExtensions.swift
│   └── UIViewExtensions.swift
├── Helpers
│   └── Constants.swift
├── Info.plist
├── InitialViewController.swift
├── Managers
│   ├── API.swift
│   └── DataParser.swift
├── Models
│   ├── Property.swift
│   └── SortOption.swift
└── Views
    ├── CollectionViewCells
    │   └── PropertyListCollectionViewCell.swift
    ├── PropertySortOptionController.swift
    └── PropertySortOptionListView.swift


## Built With

Pod list for third party libraries:

* [SnapKit](https://github.com/SnapKit/SnapKit) - used for autolayout
* [Alamofire](https://github.com/Alamofire/Alamofire) - used for network operations
* [Kingfisher](https://github.com/onevcat/Kingfisher) - used for async download image tasks


## Tests

The project include unit tests that test API and data parser functions.

