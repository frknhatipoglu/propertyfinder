//
//  PropertyListCollectionViewCell.swift
//  PropertyFinder
//
//  Created by Scorp on 20.07.2020.
//

import UIKit

class PropertyListCollectionViewCell: UICollectionViewCell {
    
    private lazy var propertyImageView: UIImageView = {
        var imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var infoStackView: UIStackView = {
        var stackView = UIStackView()
        stackView.alignment = .fill
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.spacing = 2
        return stackView
    }()
    
    private lazy var priceLabel: UILabel = {
        var label = UILabel()
        label.textAlignment = .left
        label.textColor = UIColor.black
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var infoLabel: UILabel = {
        var label = UILabel()
        label.textAlignment = .left
        label.textColor = UIColor.black
        label.numberOfLines = 0
        return label
    }()
    
    //MARK: - INITIALIZE
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureUI()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        propertyImageView.image = nil
    }
    
    //MARK: - CONFIGURE UI
    private func configureUI () {
        self.backgroundColor = UIColor.white
        
        setStackView()
        setPropertyImageView()
        
        self.layoutIfNeeded()
    }
    
    private func setStackView () {
        self.contentView.addSubview(infoStackView)
        infoStackView.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview().inset(10)
            make.bottom.equalToSuperview().inset(2)
        }
        
        infoStackView.addArrangedSubview(priceLabel)
        infoStackView.addArrangedSubview(infoLabel)
    }
    
    private func setPropertyImageView () {
        self.contentView.addSubview(propertyImageView)
        propertyImageView.snp.makeConstraints { (make) in
            make.leading.top.trailing.equalToSuperview()
            make.bottom.equalTo(infoStackView.snp.top)
        }
    }
}

//MARK: - SET UI VALUES
extension PropertyListCollectionViewCell {
    public func set (info: String?) {
        infoLabel.text = info
    }
    
    public func set (price: String?) {
        priceLabel.text = price
    }
    
    public func set (url: String?) {
        if let url = url, let urlType = URL(string: url) {
            propertyImageView.kf.setImage(with: urlType)
        }
    }
}
