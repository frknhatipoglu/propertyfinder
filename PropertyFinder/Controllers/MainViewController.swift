//
//  ViewController.swift
//  PropertyFinder
//
//  Created by Scorp on 20.07.2020.
//

import UIKit

class MainViewController: BaseViewController {
    
    private lazy var properties = [Property]()
    private lazy var totalProperyCount = 0
    private var sortOption: SortOption?
    private lazy var lastLoadedSearchPageIndex = Constants.searchApiFirstPageDefaultIndex
    private lazy var canLoadSearchData = true
    
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 20
        layout.minimumInteritemSpacing = 0
        layout.itemSize = CGSize(width: self.view.bounds.width, height: 200)
        
        let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collectionView.register(PropertyListCollectionViewCell.self, forCellWithReuseIdentifier: PropertyListCollectionViewCell.reuseIdentifier)
        collectionView.backgroundColor = UIColor.clear
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.dataSource = self
        collectionView.delegate = self
        return collectionView
    }()
    
    private lazy var propertySortOptionListView = PropertySortOptionListView()
    
    private lazy var propertySortOptionListViewController: PopupViewController = {
        let popupViewController = PopupViewController()
        popupViewController.delegate = self
        popupViewController.popupView = propertySortOptionListView
        popupViewController.modalPresentationStyle = .overFullScreen
        return popupViewController
    }()
    
    //MARK: - VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureUI()
        fetchDatas()
    }
    
    //MARK: - CONFIGURE UI
    private func configureUI () {
        self.view.backgroundColor = UIColor.Custom.MainViewController.background
        self.navigationItem.title = "Property Finder"
        
        setNavigationBarRightButton()
        setCollectionView()
        setActivityIndicatiorView()
    }
    
    private func setNavigationBarRightButton () {
        let barButtonItem = UIBarButtonItem(image: UIImage(named: "sortIcon"), style: .plain, target: self, action: #selector(sortButtonTapped(_:)))
        navigationController?.navigationBar.topItem?.rightBarButtonItem = barButtonItem
    }
    
    private func setCollectionView () {
        self.view.addSubview(collectionView)
        collectionView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    //MARK: - FETCH DATA
    private func fetchDatas () {
        fetchSearchData()
    }
    
    private func fetchSearchData (sortOption: SortOption? = nil, pageIndex: Int? = nil) {
        
        if !self.canLoadSearchData {
            return
        }
        
        self.activityIndicatorView.startAnimating()
        self.canLoadSearchData = false
        
        API.search(sortOption: sortOption?.rawValue, pageIndex: pageIndex) { (responseData, error) in
            
            if let _ = error {
                self.presentAlertView()
                self.activityIndicatorView.stopAnimating()
                self.canLoadSearchData = true
                return
            }
            
            guard let responseData = responseData else {
                self.presentAlertView()
                self.activityIndicatorView.stopAnimating()
                self.canLoadSearchData = true
                return
            }
            
            DataParser.parse(searchData: responseData) { (searchResponse) in
                self.properties.append(contentsOf: searchResponse?.res ?? [])
                self.totalProperyCount = searchResponse?.total ?? 0
                self.collectionView.reloadData()
                self.activityIndicatorView.stopAnimating()
                self.lastLoadedSearchPageIndex += 1
                self.canLoadSearchData = true
            }
        }
    }
    
    //MARK: - ACTIONS
    @objc private func sortButtonTapped (_ sender: UIBarButtonItem) {
        propertySortOptionListView.set(currentSortOption: sortOption)
        self.present(propertySortOptionListViewController, animated: true, completion: nil)
    }
    
    //MARK: - HELPERS
    private func clearCollectionViewData () {
        lastLoadedSearchPageIndex = Constants.searchApiFirstPageDefaultIndex
        properties.removeAll()
        collectionView.reloadData()
    }
}

//MARK: - Presenters
extension MainViewController {
    private func presentAlertView () {
        let alertController = UIAlertController(title: "An error ocurred", message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "Close", style: .default, handler: nil)
        
        alertController.addAction(action)
        self.present(alertController, animated: true, completion: nil)
    }
}

//MARK: - CollectionView Methods
extension MainViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if indexPath.item == (properties.count - 1)  && indexPath.item < totalProperyCount {
            fetchSearchData(sortOption: sortOption, pageIndex: lastLoadedSearchPageIndex + 1)
        }
    }
}

extension MainViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return properties.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PropertyListCollectionViewCell.reuseIdentifier, for: indexPath) as! PropertyListCollectionViewCell
        
        return configure(cell: cell, indexPath: indexPath)
    }
}

extension MainViewController {
    private func configure (cell: PropertyListCollectionViewCell, indexPath: IndexPath) -> PropertyListCollectionViewCell {
        
        let cellData = properties[indexPath.item]
        
        cell.set(info: cellData.title)
        cell.set(url: cellData.thumbnailBig)
        
        if let price = cellData.price, let currency = cellData.currency {
            cell.set(price: price + " " + currency)
        }
        
        return cell
    }
}

//MARK: - PopupViewControllerDelegate
extension MainViewController: PopupViewControllerDelegate {
    func popupViewControllerWillDismiss() {
        let newSortOption = propertySortOptionListView.getCurrentSortOption()
        
        if sortOption == newSortOption {
            return
        }
        
        clearCollectionViewData()
        sortOption = newSortOption
        fetchSearchData(sortOption: newSortOption)
    }
}
