//
//  UIViewExtensions.swift
//  PropertyFinder
//
//  Created by Scorp on 20.07.2020.
//

import UIKit

extension UIView {
    // create unique reuse identifier with class name (because: each class name is unique)
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}
